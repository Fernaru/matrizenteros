package com.es.camino.enteros.utils;

public enum Position{

	ARRIBA((i, j) -> 1, 0),
	DERECHA((i, j) -> 1, 0),
	ABAJO((i, j) -> 1, 0),
	IZQUIERDA((i, j) -> 1, 0);
	
	
	private Operation op;
	Position(Operation operation) {
	this.op = operation;
	}
	
	Position(Operation operation, int i) {
		this.op = operation;
	}

	public int apply(int i, int j) {
			return op.apply(i, j); 
	}

	@FunctionalInterface
	public static interface Operation {
	int apply(int i, int j);
	}
}
