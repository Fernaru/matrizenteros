package com.es.camino.enteros.dto;

import java.util.Arrays;

import lombok.Data;

@Data
public class Section {

	private Integer posX;
	private Integer posY;
	private boolean left = true;
	private boolean rigth = true;
	private boolean fin = true;
	private boolean visited = false;
	private int value;
	private boolean paso[];// arriba = 0, 1 = derecha, 2 = abajo, 3 = izquierda

	public Section(Integer posX, Integer posY, boolean left, boolean rigth, boolean fin, boolean[] paso) {
		this.posX = posX;
		this.posY = posY;
		this.left = left;
		this.rigth = rigth;
		this.fin = fin;
		this.visited = false;
		this.paso = paso;
	}

	public Section(int posX, int posY, int value, boolean[] paso, boolean fin) {
		this.posX = posX;
		this.posY = posY;
		this.fin = fin;
		this.visited = false;
		this.paso = paso;
		this.value = value;
	}

	public boolean upAvailable() {
        return paso[0];
    }

    public boolean rightAvailable() {
        return paso[1];
    }

    public boolean downAvailable() {
        return paso[2];
    }

    public boolean leftAvailable() {
        return paso[3];
    }

    @Override
    public String toString() {
        return "X=" + posX + ", Y=" + posY;
    }
	
	@Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Section other = (Section) obj;
        if (this.posX != other.posX) {
            return false;
        }
        if (this.posY != other.posY) {
            return false;
        }
        return true;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(paso);
		result = prime * result + ((posX == null) ? 0 : posX.hashCode());
		result = prime * result + ((posY == null) ? 0 : posY.hashCode());
		return result;
	}

}
