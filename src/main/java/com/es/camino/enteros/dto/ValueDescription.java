package com.es.camino.enteros.dto;

import lombok.Data;

@Data
public class ValueDescription {

	private Long value;
	private String paths;
}
