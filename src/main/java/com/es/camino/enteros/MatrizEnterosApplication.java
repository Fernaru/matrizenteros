package com.es.camino.enteros;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MatrizEnterosApplication {

	public static void main(String[] args) {
		SpringApplication.run(MatrizEnterosApplication.class, args);
	}

}
