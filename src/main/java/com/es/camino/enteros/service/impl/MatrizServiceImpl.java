package com.es.camino.enteros.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.stereotype.Service;

import com.es.camino.enteros.controller.dto.Response;
import com.es.camino.enteros.dto.Section;
import com.es.camino.enteros.dto.ValueDescription;
import com.es.camino.enteros.service.IMatrizService;

@Service
public class MatrizServiceImpl implements IMatrizService {

	private Section[][] A;
	private ArrayList<ArrayList<Section>> paths;

	public MatrizServiceImpl(Section[][] A) {
		this.A = A;
		this.paths = new ArrayList<>();
	}

	@Override
	public Response getIntegerPath(int n, int m) {
		A = new Section[n][m];
		A = generateMatriz(A, n, m);
		return readMatriz(A, n, m);
	}

	private Section[][] generateMatriz(Section[][] A, int n, int m) {
		for (int x = 0; x < n; x++) {
			System.out.print("| ");
			for (int y = 0; y < m; y++) {
				if (x == (n - 1) && y == (m - 1)) {
					A[x][y] = new Section(x, y, (int) Math.round((Math.random() * 9) + 1) * 10,
							new boolean[] { true, true, true, true }, true);
					System.out.print(A[x][y].getValue());
					System.out.print(" ");
				} else {
					A[x][y] = new Section(x, y, (int) Math.round((Math.random() * 9) + 1) * 10,
							new boolean[] { true, true, true, true }, false);
					System.out.print(A[x][y].getValue());
					System.out.print(" ");
				}
			}
			System.out.println("|");
		}
		return A;
	}

	private Response readMatriz(Section[][] A, int n, int m) {
		Random r = new Random();
		int fila = 0;
		int columna = 0;
		for (int i = 0; i < 1; i++) {
			fila = r.nextInt(A.length);
			columna = r.nextInt(A[0].length);
		}
		// Posible camino que puede haber
		ArrayList<Section> camino = new ArrayList<>();
		// Creo el laberinto
		MatrizServiceImpl mat = new MatrizServiceImpl(A);
		camino.add(A[0][0]);
		A[fila][columna].setVisited(true);
		fillPaths(mat, A[fila][columna], camino);
		// Muestro las soluciones
		return mat.showPaths(n, m);
	}

	public static void fillPaths(MatrizServiceImpl mat, Section casillaActual, ArrayList<Section> camino) {

		if (casillaActual.isFin()) {

			mat.addPath((ArrayList<Section>) camino.clone());

		} else {

			int[][] movimientos = { { -1, 0 }, { 0, 1 }, { 1, 0 }, { 0, -1 } };

			int posXnueva, posYnueva;
			Section aux;
			boolean accesible = false;

			for (int i = 0; i < movimientos.length; i++) {

				posXnueva = casillaActual.getPosX() + movimientos[i][0];
				posYnueva = casillaActual.getPosY() + movimientos[i][1];
				aux = mat.getCasillaAt(posXnueva, posYnueva);

				switch (i) {
				case 0:
					accesible = mat.upAvailable(casillaActual, aux);
					break;
				case 1:
					accesible = mat.rightAvailable(casillaActual, aux, casillaActual.isRigth());
					break;
				case 2:
					accesible = mat.downAvailable(casillaActual, aux);
					break;
				case 3:
					accesible = mat.leftAvailable(casillaActual, aux, casillaActual.isLeft());
					break;
				}

				if (accesible) {
					camino.add(aux);
					casillaActual.setVisited(true);
					fillPaths(mat, aux, camino);
					casillaActual.setVisited(false);
					camino.remove(aux);
				}
			}
		}
	}

	public boolean upAvailable(Section casillaActual, Section casillaDestino) {

		if (casillaDestino != null && !casillaDestino.isVisited()) {
			return casillaActual.upAvailable();
		}
		return false;
	}

	public boolean rightAvailable(Section casillaActual, Section casillaDestino, boolean right) {
		if (casillaDestino != null && !casillaDestino.isVisited() && right) {
			right = false;
			return casillaActual.rightAvailable();
		}
		return false;
	}

	public boolean downAvailable(Section casillaActual, Section casillaDestino) {
		if (casillaDestino != null && !casillaDestino.isVisited()) {
			return casillaActual.downAvailable();
		}
		return false;
	}

	public boolean leftAvailable(Section casillaActual, Section casillaDestino, boolean left) {
		if (casillaDestino != null && !casillaDestino.isVisited() && left) {
			left = false;
			return casillaActual.leftAvailable();
		}
		return false;
	}

	public Section getCasillaAt(int x, int y) {
		if (isLimit(x, y)) {
			return A[x][y];
		}
		return null;
	}

	public boolean isLimit(int x, int y) {
		return (x >= 0 && x < A.length) && (y >= 0 && y < A[0].length);
	}

	public void addPath(ArrayList<Section> camino) {
		if (camino != null && !camino.isEmpty()) {
			paths.add(camino);
		}
	}

	public Response showPaths(int n, int m) {
		List<ValueDescription> valueList = new ArrayList<>();
		ValueDescription value = null;
		long total = 1;
		int i = 1;
		for (ArrayList<Section> camino : paths) {
			value = new ValueDescription();
			value.setPaths("Camino: " + 1);
			for (Section c : camino) {
				total = total * c.getValue();

			}
			value.setValue(total);
			total = 1;
			valueList.add(value);
			i++;
		}
		return getValueList(valueList, n, m);
	}

	private Response getValueList(List<ValueDescription> valueList, int n, int m) {
		int cont = 0;
		int contTmp = 0;
		long value = 0;
		long valTmp = 0;
		
		for (ValueDescription v : valueList) {
			char[] arrayChar = v.getValue().toString().toCharArray();
			if (cont > contTmp) {
				contTmp = cont;
				if (valTmp > v.getValue()) {
					value = valTmp;
				} else {
					value = v.getValue();
				}
			}
			cont = 0;
			for (int h = 0; h < arrayChar.length; h++) {
				if (arrayChar[h] == '0') {
					cont = cont + 1;
				}
			}
			valTmp = v.getValue();
		}
		return validValue(value, n, m, contTmp);
	}

	private Response validValue(long value, int n, int m, int contTmp) {
		if (value == 0) {
			return getIntegerPath(n, m);
		}
		return Response.builder().value(value).cantCezor(contTmp).build();
	}

}
