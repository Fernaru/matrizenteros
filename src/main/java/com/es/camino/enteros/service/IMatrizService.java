package com.es.camino.enteros.service;

import com.es.camino.enteros.controller.dto.Response;

public interface IMatrizService {

	public Response getIntegerPath(int n, int m);

}
