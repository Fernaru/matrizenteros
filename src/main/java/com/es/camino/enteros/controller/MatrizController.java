package com.es.camino.enteros.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.es.camino.enteros.controller.dto.Response;
import com.es.camino.enteros.service.IMatrizService;

@RestController
@RequestMapping(value="/matriz")
public class MatrizController {

	@Autowired
	private IMatrizService service;
	
	@GetMapping(value="/integer")
	public Response getIntegerPath(@RequestParam(name = "N") int N, @RequestParam(name = "M") int M){
		Response responsePathMatriz = service.getIntegerPath(N, M);
		return responsePathMatriz;
	}
}
