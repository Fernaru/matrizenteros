package com.es.camino.enteros.controller.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Response {

	private long value;
	private long cantCezor;
}
